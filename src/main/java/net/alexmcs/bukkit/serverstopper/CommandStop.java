package net.alexmcs.bukkit.serverstopper;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * ServerStopper
 * net.alexmcs.bukkit.serverstopper.CommandStop
 * Command handler for /stop.
 * This class handles all the main commands that interact with StopManager to stop the server.
 */
class CommandStop implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        // Determine if the stop request was sent by a player or the console and parse their username
        String senderName;
        if (sender instanceof Player) {
            Player player = (Player) sender;
            senderName = player.getDisplayName();
        } else if (sender == Bukkit.getConsoleSender()) {
            senderName = Translations.consoleDisplayname;
        } else {
            Bukkit.getLogger().warning("An invalid sender has issued the /stop command.");
            senderName = "??????";
        }

        // Parse the first argument
        String arg1;
        if(args.length == 0) {
            arg1 = "";
        } else {
            arg1 = args[0];
        }

        // The cancel command cancels a pending server stop. This is done by setting the clock in the StopManager class to -1.
        if (arg1.equalsIgnoreCase("cancel") || arg1.equalsIgnoreCase("-1")) {
            if (sender.hasPermission("serverstopper.cancel")) {
                // Check if the clock is already -1. If so, the server should not be stopping, so there's nothing to cancel.
                if (StopManager.clock == -1) {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorNotStopping));
                } else {
                    // Send a call to the StopManager.initStop() method to cancel the stop.
                    StopManager.initStop(sender, senderName, -1);
                }
            } else {
                if (!ConfigManager.genericPermissionErrors) {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorPermissionCancel));
                } else {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorPermission));
                }
            }
            return true;
        }

        // The help command is a simple alias for /serverstopper help
        else if (arg1.equalsIgnoreCase("help")) {
            Bukkit.dispatchCommand(sender, "serverstopper:serverstopper help");
            return true;
        }

        // For any other command, permission for serverstopper.stop is checked first. I know this seems odd, but it makes for a more efficient workflow.
        else if (sender.hasPermission("serverstopper.stop")) {

            // The now command will stop the server immediately. This is done by dispatching minecraft:stop from the console immediately.
            if (arg1.equalsIgnoreCase("now") || arg1.equalsIgnoreCase("0")) {
                if (sender.hasPermission("serverstopper.stop.now")) {
                    StopManager.doServerStop();
                } else {
                    if (!ConfigManager.genericPermissionErrors) {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorPermissionNow));
                    } else {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorPermission));
                    }
                }
                return true;
            }

            // If the first argument is an integer, the server will be scheduled to stop in that many seconds times the default multiplier. This is done by multiplying the integer by ConfigManager.defaultMultiplier and setting the clock in the StopManager class to that value.
            else if (isInteger(arg1)) {
                if (sender.hasPermission("serverstopper.stop.time")) {
                    int stopTime = Integer.valueOf(arg1) * ConfigManager.defaultMultiplier;
                    // See doInitStop()
                    doInitStop(sender, senderName, stopTime);
                } else {
                    if (!ConfigManager.genericPermissionErrors) {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorPermissionTime));
                    } else {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorPermission));
                    }
                }
                return true;
            }

            // If no first argument is provided, the server will be scheduled to stop with the default delay. This is done by setting the clock in the StopManager class to ConfigManager.defaultDelay.
            else if (arg1.equalsIgnoreCase("")) {
                // See doInitStop()
                doInitStop(sender, senderName, ConfigManager.defaultDelay);
                return true;
            }

            // Error for unknown commands.
            else {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorUnknownCommand));
                return false;
            }

        } else { // this is the else from the serverstopper.stop permission check way up there. :)
            if (!ConfigManager.genericPermissionErrors) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorPermissionStop));
            } else {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorPermission));
            }
            return true;
        }

    }

    // This method seems a little redundant, but I felt it was the most effective way to handle the error detailed below.
    private static void doInitStop(CommandSender sender, String senderName, int stopTime) {

        // If the server is already stopping (this is determined by checking if the clock in StopManager class is any other value than -1), display an error.
        if (StopManager.clock != -1) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorAlreadyStopping));
        } else if (stopTime > ConfigManager.minDelay) {
            // Send a call to the StopManager.initStop() method to stop the server in the given number of seconds.
            StopManager.initStop(sender, senderName, stopTime);
        } else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorTooShort).replace("{count}", Integer.toString(ConfigManager.minDelay)));
        }

    }

    // This basically does some math to determine if a given string can be safely converted to an integer.
    private static boolean isInteger(String s) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),10) < 0) return false;
        }
        return true;
    }

}
