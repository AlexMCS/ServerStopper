package net.alexmcs.bukkit.serverstopper;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * ServerStopper
 * net.alexmcs.bukkit.serverstopper.CommandCancelstop
 * Command handler for /cancelstop.
 * Cancels a server stop. This method is depreciated and /stop cancel should be used instead.
 */
class CommandCancelstop implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        // Verify that there are no arguments.
        if (args.length == 0) {
            // Alias to serverstopper:stop cancel
            Bukkit.dispatchCommand(sender, "serverstopper:stop cancel");
            return true;
        }

        // Error for unknown commands.
        else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4&lERROR&r: Unknown command."));
            return false;
        }

    }

}
