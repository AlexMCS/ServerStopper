package net.alexmcs.bukkit.serverstopper;

import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * ServerStopper
 * net.alexmcs.bukkit.serverstopper.MainClass
 * Main class for ServerStopper
 */
public class MainClass extends JavaPlugin implements Listener {

    // This method is called when the plugin is enabled. (duh!) It registers the commands and does some other cool stuff.
    // There is a little bit of processing done here, but much of it is just spitting out information to the console.
    static Plugin thisPlugin;
    @Override
    public void onEnable() {

        thisPlugin = this;

        // Set up commands (registration is done in plugin.yml)
        this.getCommand("stop").setExecutor(new CommandStop());
        this.getCommand("cancelstop").setExecutor(new CommandCancelstop());
        this.getCommand("serverstopper").setExecutor(new CommandServerstopper());

        // Load the configuration
        ConfigManager.loadConfiguration();

        // Informational
        getLogger().info("Enabled ServerStopper");
        getLogger().info(Translations.pluginName + " v" + Translations.version);
        getLogger().info("for Bukkit/Spigot/Paper");
        getLogger().info("ServerStopper replaces Minecraft's /stop command with a better version that warns users before the server shuts down.");
        getLogger().info(Translations.website);

    }

    // This method is called when the plugin is disabled. (i.e. when the server is shutting down) I only use it for informational purposes here.
    @Override
    public void onDisable() {
        getLogger().info("The server is shutting down.");
        getLogger().info("Thank you for using ServerStopper! Goodbye.");
        getLogger().info("Disabled ServerStopper");
    }

}
