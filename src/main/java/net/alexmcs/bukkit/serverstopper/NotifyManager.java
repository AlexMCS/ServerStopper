package net.alexmcs.bukkit.serverstopper;

import io.puharesource.mc.titlemanager.api.ActionbarTitleObject;
import io.puharesource.mc.titlemanager.api.TabTitleObject;
import io.puharesource.mc.titlemanager.api.TitleObject;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.inventivetalent.bossbar.BossBar;
import org.inventivetalent.bossbar.BossBarAPI;

import static net.alexmcs.bukkit.serverstopper.MainClass.thisPlugin;

/**
 * ServerStopper
 * net.alexmcs.bukkit.serverstopper.NotifyManager
 * The class that handles chat, titles, bossbars, and all other ways of notifying players of an impending stop.
 * NotifyManager does not currently support any alternative chat APIs
 */
class NotifyManager {

    // Should this entire method be sync-wrapped?
    static void doWarn(int time) {

        // Since this needs to be used on the synchronous thread, this finalized string is required to handle the inner class issue.
        String countTmp;
        String timeunitsTmp;

        if (ConfigManager.calculateTimeunits) {
            if (time % 60 == 0) {
                countTmp = Integer.toString(time/60);
                timeunitsTmp = "minutes";
            } else if (time % 3600 == 0) {
                countTmp = Integer.toString(time/3600);
                timeunitsTmp = "hours";
            } else {
                countTmp = Integer.toString(time);
                timeunitsTmp = "seconds";
            }
        } else {
            countTmp = Integer.toString(time);
            timeunitsTmp = "seconds";
        }

        final String count = countTmp;
        final String timeunits = timeunitsTmp;
        final int clock = time;

        // Run this synchronously. These APIs *probably* aren't thread safe. :)
        new BukkitRunnable(){
            @Override
            public void run() {

                // Chat
                if (ConfigManager.warningIntervals.contains(clock) || (clock <= ConfigManager.warningCountdown && clock > 0)) {
                    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', Translations.broadcastPrefix + Translations.broadcastWarning.replace("{count}", count).replace("{timeunits}", timeunits)));
                }

                // BossBarAPI
                if (ConfigManager.apiBossbar.equals("BossBarAPI")) {
                    BossBar bossBar = BossBarAPI.addBar(new TextComponent(ChatColor.translateAlternateColorCodes('&', Translations.bossbarWarning.replace("{count}", count).replace("{timeunits}", timeunits))), BossBarAPI.Color.valueOf(ConfigManager.bossbarSimpleColor), BossBarAPI.Style.PROGRESS, (float)clock/ConfigManager.bossbarSimpleCountdown);
                    for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
                        if (clock <= ConfigManager.bossbarSimpleCountdown && clock > -1) {
                            BossBarAPI.removeAllBars(pl);
                            bossBar.addPlayer(pl);
                        } else if (clock == -1) {
                            BossBarAPI.removeAllBars(pl);
                        }
                    }
                }

                // TitleManager API
                if (ConfigManager.apiTitles.equals("TitleManager")) {
                    String tmSubtitle = "";
                    String tmTitle = "";
                    boolean doSetTitle = false;
                    if (ConfigManager.tmAbEnable) {
                        if (ConfigManager.tmAbIntervals.contains(clock) || (clock <= ConfigManager.tmAbCountdownMax && clock >= ConfigManager.tmAbCountdownMin)) {
                            ActionbarTitleObject actionbarTitleObject = new ActionbarTitleObject(ChatColor.translateAlternateColorCodes('&', Translations.tmAbWarning.replace("{count}", count).replace("{timeunits}", timeunits)));
                            actionbarTitleObject.broadcast();
                        }
                    }
                    if (ConfigManager.tmSubEnable) {
                        if (ConfigManager.tmSubIntervals.contains(clock) || (clock <= ConfigManager.tmSubCountdownMax && clock >= ConfigManager.tmSubCountdownMin)) {
                            tmSubtitle = ChatColor.translateAlternateColorCodes('&', Translations.tmSubWarning.replace("{count}", count).replace("{timeunits}", timeunits));
                            doSetTitle = true;
                        }
                    }
                    if (ConfigManager.tmTitleEnable) {
                        if (clock <= ConfigManager.tmTitleCountdownMax && clock >= ConfigManager.tmTitleCountdownMin) {
                            tmTitle = ChatColor.translateAlternateColorCodes('&', Translations.tmTitleWarning.replace("{count}", count).replace("{timeunits}", timeunits));
                            doSetTitle = true;
                        }
                    }
                    if (doSetTitle) {
                        TitleObject titleObject = new TitleObject(tmTitle, tmSubtitle);
                        titleObject.broadcast();
                    }
                    if (ConfigManager.tmTabfooterEnable) {
                        TabTitleObject tabTitleObject = new TabTitleObject(ChatColor.translateAlternateColorCodes('&', ConfigManager.titleManagerTabHeader), ChatColor.translateAlternateColorCodes('&', Translations.tmTabFooterWarning.replace("{count}", count).replace("{timeunits}", timeunits)));
                        tabTitleObject.broadcast();
                    }
                }

            }

        }.runTask(thisPlugin);

    }

}
