package net.alexmcs.bukkit.serverstopper;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import static net.alexmcs.bukkit.serverstopper.MainClass.thisPlugin;

/**
 * ServerStopper
 * net.alexmcs.bukkit.serverstopper.CommandServerstopper
 * Command handler for /serverstopper.
 * Gives information about ServerStopper and help on how to use it.
 */
class CommandServerstopper implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        // Parse the first argument
        String arg1;
        if(args.length == 0) {
            arg1 = "";
        } else {
            arg1 = args[0];
        }

        // Handle the reload command
        if (arg1.equalsIgnoreCase("reload")) {
            if (sender.hasPermission("serverstopper.reload")) {
                // Call ConfigManager.loadConfiguration() again.
                ConfigManager.loadConfiguration();
                thisPlugin.getLogger().info("ServerStopper configuration successfully reloaded by " + sender.getName());
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.reloadSuccess));
            } else {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorPermission));
            }

            return true;
        }

        // The info command spits out some basic information about ServerStopper. Nothing too interesting here.
        else if (arg1.equalsIgnoreCase("info") || arg1.equalsIgnoreCase("version") || arg1.equalsIgnoreCase("")) {
            if (sender.hasPermission("serverstopper.info")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&lServer&4&lStopper&a&l v" + thisPlugin.getDescription().getVersion()));
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "By &2&lAlex&6&lMCS&r / &b&lColton&4&lDRG&r"));
                if (!Translations.aboutTranslator.equals("")) {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.aboutTranslator));
                }
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.aboutHelp));
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.aboutUri));
            } else {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorPermission));
            }
            return true;
        }

        // The help command spits out information on how to properly use ServerStopper. Nothing too interesting here.
        else if (arg1.equalsIgnoreCase("help")) {
            if (sender.hasPermission("serverstopper.help")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&lServer&4&lStopper&a&l v" + thisPlugin.getDescription().getVersion()));
                // PREPARE FOR THE MOST INSANE NESTED IF YOU'VE EVER SEEN!!!!!!!!!!!
                if (!Translations.help1.equals("")) {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.help1));
                    if (!Translations.help2.equals("")) {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.help2));
                        if (!Translations.help3.equals("")) {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.help3));
                            if (!Translations.help4.equals("")) {
                                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.help4));
                                if (!Translations.help5.equals("")) {
                                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.help5));
                                    if (!Translations.help6.equals("")) {
                                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.help6));
                                        if (!Translations.help7.equals("")) {
                                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.help7));
                                            if (!Translations.help8.equals("")) {
                                                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.help8));
                                                if (!Translations.help9.equals("")) {
                                                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.help9));
                                                    if (!Translations.help10.equals("")) {
                                                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.help10));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorPermission));
            }
            return true;
        }

        // Error for unknown commands.
        else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.errorPrefix + Translations.errorUnknownCommand));
            return false;
        }
    }



}
