package net.alexmcs.bukkit.serverstopper;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import static net.alexmcs.bukkit.serverstopper.MainClass.thisPlugin;

/**
 * ServerStopper
 * net.alexmcs.bukkit.serverstopper.StopManager
 * ServerStopper StopManager class.
 * This class is the meat and potatoes of ServerStopper. It does all the heavy lifting.
 */
class StopManager {

    // Defining the clock for later use. ;3
    static int clock = -1;

    // The grand finale. https://youtu.be/j0h2u87JwyA (The method that does all the dirty work)
    private static void stopLoop() {
        // Not really sure why I try wrapped the whole thing. Whatever.
        // This loop runs as long as the clock is not "stopped", which is an integer equal to -1.
        while (clock != -1) {
            // Let people know how much time is left (calculations are handled by NotifyManager)
            NotifyManager.doWarn(clock);
            // If the clock is 0, stop the server.
            if (clock == 0) {
                doServerStop();
            }
            // Subtract one from the clock...
            clock = clock - 1;
            // ... and wait one second before looping again.
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }

    // This method is called by the other classes to actually process any operations.
    static void initStop(CommandSender sender, String displayName, int time) {

        // If the integer passed for time is -1, the user intended to cancel the pending stop.
        if (time == -1) {
            if (clock != -1) {
                // Tell all server users that the stop was canceled.
                if (ConfigManager.notify) {
                    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', Translations.broadcastPrefix + Translations.broadcastCancel.replace("{displayname}", displayName)));
                } else {
                    thisPlugin.getLogger().info(ChatColor.translateAlternateColorCodes('&', Translations.broadcastCancel.replace("{displayname}", displayName)));
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.broadcastCancel.replace("{displayname}", displayName)));
                }
                // Cancel the stop. Simply setting the clock to -1 is fine, as this will abort the while loop.
                clock = -1;
                // Let NotifyManager know so it can clean up the bossbar and other junk.
                NotifyManager.doWarn(clock);
            }
        }

        // Schedule a server stop.
        else {
            // Notify all server users that someone has scheduled a stop.
            if (ConfigManager.notify) {
                Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', Translations.broadcastPrefix + Translations.broadcastNotify.replace("{displayname}", displayName).replace("{count}", Integer.toString(time)).replace("{timeunits}", "seconds")));
            } else {
                thisPlugin.getLogger().info(ChatColor.translateAlternateColorCodes('&', Translations.broadcastNotify.replace("{displayname}", displayName).replace("{count}", Integer.toString(time)).replace("{timeunits}", "seconds")));
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Translations.broadcastNotify.replace("{displayname}", displayName).replace("{count}", Integer.toString(time)).replace("{timeunits}", "seconds")));
            }
            // Set the clock to the integer passed for time. This will reflect seconds in real world time.
            clock = time;
            // We run the while loop asynchronously. This is because we do a lot of Thread.sleep() in order to determine how much real world time has passed. Obviously, doing this on the main thread would hang the server and make it unresponsive.
            new BukkitRunnable() {
                @Override
                public void run() {
                    // Wait one second
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    // Call the stopLoop() method to get the while loop started.
                    stopLoop();
                }
            }.runTaskAsynchronously(thisPlugin);
        }
    }

    static void doServerStop() {
        // Get back onto the main thread. Accessing the Bukkit API from asynchronous threads is *super dangerous*!
        new BukkitRunnable(){
            @Override
            public void run() {
                // Dispatch the "minecraft:stop" command from the console.
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "minecraft:stop");
            }
        }.runTask(thisPlugin);
    }


}
