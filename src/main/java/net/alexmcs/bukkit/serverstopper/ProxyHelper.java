package net.alexmcs.bukkit.serverstopper;

/**
 * ServerStopper
 * net.alexmcs.bukkit.serverstopper.ProxyHelper
 * This class is designed for ProxyStopper for BungeeCord to communicate with ServerStopper for Bukkit-y tasks.
 * This is required to make use of BossBarAPI and TitleManager for ProxyStopper as those things can't run on the proxy.
 * This will be implemented in a future version of ProxyStopper.
 */
class ProxyHelper {
}
