package net.alexmcs.bukkit.serverstopper;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.util.List;

import static net.alexmcs.bukkit.serverstopper.MainClass.thisPlugin;

/**
 * ServerStopper
 * net.alexmcs.bukkit.serverstopper.ConfigManager
 * The class that deals with loading the config and making magic happen.
 */
class ConfigManager {

    // See the commented node in the bukkit-config.yml resource for more information on each option.

    // Basic options
    static int defaultDelay; // delay
    static int defaultMultiplier; // delay-multiplier
    static int minDelay; // min-delay
    static boolean genericPermissionErrors; // generic-permission-errors

    // Normal chat warnings options
    static List<Integer> warningIntervals; // warnings.intervals
    static int warningCountdown; // warnings.countdown

    // BossBarAPI options
    private static boolean bossbarEnable; // bossbar.enable
    static int bossbarSimpleCountdown; // bossbar.countdown
    static String bossbarSimpleColor; // bossbar.color
    static boolean bossbarGhetto; // bossbar.ghetto
    // Add colourshift options later.

    // TitleManager API options
    private static boolean tmEnable; // titles.enable
    static boolean tmAbEnable; // titles.actionbar.enable
    static List<Integer> tmAbIntervals; // titles.actionbar.intervals
    static int tmAbCountdownMax; // titles.actionbar.max
    static int tmAbCountdownMin; // titles.actionbar.min
    static boolean tmSubEnable; // titles.subtitle.enable
    static List<Integer> tmSubIntervals; // titles.subtitle.intervals
    static int tmSubCountdownMax; // titles.subtitle.max
    static int tmSubCountdownMin; // titles.subtitle.min
    static boolean tmTitleEnable; // titles.title.enable
    static int tmTitleCountdownMax; // titles.title.max
    static int tmTitleCountdownMin; // titles.title.min
    static boolean tmTabfooterEnable; // titles.tabfoot

    // ProxyHelper options
    static boolean phEnable; // bungeecord.enable
    static boolean phBossbarEnable; // bungeecord.bossbar
    static boolean phTmEnable; // bungeecord.titles
    static boolean phTmTabfooterEnable; // bungeecord.tabfoot

    // Experimental options
    static boolean notify; // notify
    static boolean calculateTimeunits; // calculate-timeunits
    static boolean allowMsg; // allow-msg

    // Config version (used only internally)
    private static String configVersion; // config-version

    // More internals
    static String apiBossbar;
    static String apiTitles;

    // TitleManager internals
    static boolean titleManagerTabHeaderEnabled;
    static String titleManagerTabHeader;


    static void loadConfiguration() {

        thisPlugin.getDataFolder().mkdirs(); // This creates the config directory if it doesn't already exist. Using an if like done below for config.yml is probably more proper, but this works fine and I'm lazy.
        File configFile = new File(thisPlugin.getDataFolder(), "config.yml");

        // The ability to focus is something I do not possess.

        // If the configuration does not exist, create it.
        if (!configFile.exists()) {
            thisPlugin.getLogger().warning("The config file does not exist. Creating it.");
            try {
                doExportResource("bukkit-config.yml", "config.yml");
            } catch (Exception ex) {
                throw new RuntimeException("Unable to export resource", ex);
            }

        }

        FileConfiguration config = new YamlConfiguration().loadConfiguration(configFile);

        // Basic options
        defaultDelay = config.getInt("delay", 60);
        defaultMultiplier = config.getInt("delay-multiplier", 60);
        minDelay = config.getInt("min-delay", 30);

        // Normal chat warnings options
        warningIntervals = config.getIntegerList("warnings.intervals");
        warningCountdown = config.getInt("warnings.countdown", 10);

        // BossBarAPI options
        bossbarEnable = config.getBoolean("bossbar.enable", false);
        bossbarSimpleCountdown = config.getInt("bossbar.countdown", 60);
        bossbarSimpleColor = config.getString("bossbar.color", "RED");

        // TitleManager API options
        tmEnable = config.getBoolean("titles.enable", false);
        tmAbEnable = config.getBoolean("titles.actionbar.enable", true);
        tmAbIntervals = config.getIntegerList("titles.actionbar.intervals");
        tmAbCountdownMax = config.getInt("titles.actionbar.max", 60);
        tmAbCountdownMin = config.getInt("titles.actionbar.min", 16);
        tmSubEnable = config.getBoolean("titles.subtitle.enable", true);
        tmSubIntervals = config.getIntegerList("titles.subtitle.intervals");
        tmSubCountdownMax = config.getInt("titles.subtitle.max", 15);
        tmSubCountdownMin = config.getInt("titles.subtitle.min", 0);
        tmTitleEnable = config.getBoolean("titles.title.enable", true);
        tmTitleCountdownMax = config.getInt("titles.title.max", 10);
        tmTitleCountdownMin = config.getInt("titles.title.min", 0);
        tmTabfooterEnable = config.getBoolean("titles.tabfoot");

        // ProxyHelper options
        phEnable = config.getBoolean("bungeecord.enable", false);
        phBossbarEnable = config.getBoolean("bungeecord.bossbar", true);
        phTmEnable = config.getBoolean("bungeecord.titles", true);
        phTmTabfooterEnable = config.getBoolean("bungeecord.tabfoot", true);

        // Experimental options
        notify = config.getBoolean("notify", true);
        calculateTimeunits = config.getBoolean("calculate-timeunits", false);
        allowMsg = config.getBoolean("allow-msg", false);

        // Config version (used only internally)
        configVersion = config.getString("config-version", "NULL");

        // Throw a warning if configVersion is different.
        if (configVersion.equals("SNAPSHOT")) {
            thisPlugin.getLogger().warning("Your configuration was generated by an unstable SNAPSHOT!");
            thisPlugin.getLogger().warning("SNAPSHOT configurations are often undergoing big changes and may be invalid.");
            thisPlugin.getLogger().warning("If you see problems or ignored values, backup and delete your configuration to let the plugin regenerate it.");
        } else if (configVersion.equals("NULL")) {
            thisPlugin.getLogger().warning("Your configuration version could not be detected!");
            thisPlugin.getLogger().warning("Your configuration may be invalid.");
            thisPlugin.getLogger().warning("If you see problems or ignored values, backup and delete your configuration to let the plugin regenerate it.");
        } else if (!configVersion.equals("10")) {
            thisPlugin.getLogger().warning("Your configuration version is invalid!");
            thisPlugin.getLogger().warning("Your configuration may be invalid.");
            thisPlugin.getLogger().warning("If you see problems or ignored values, backup and delete your configuration to let the plugin regenerate it.");
        }

        // Setup the various APIs
        apiSetup();

        // Load translations
        Translations.loadTranslations();

    }

    // This method is in charge of detecting the other available APIs (if enabled) that the plugin supports and setting the plugin up for them.
    private static void apiSetup() {

        // BossBarAPI setup
        if (bossbarEnable) {
            if (Bukkit.getPluginManager().isPluginEnabled("BossBarAPI")) {
                thisPlugin.getLogger().info("BossBarAPI is available. Setting it up.");
                apiBossbar = "BossBarAPI";
            } else {
                thisPlugin.getLogger().warning("BossBarAPI is not installed. If you don't want to use the BossBar, you can ignore this. You can suppress this warning by setting bossbar.enable: false in config.yml");
                thisPlugin.getLogger().warning("If you need a version of BossBarAPI that works with the latest version of Minecraft, you can get it here: https://git.drg.li/AlexMCS/BossBarAPI/releases");
                apiBossbar = "none";
            }
        } else {
            apiBossbar = "none";
        }

        // TitleManager API setup
        if (tmEnable) {
            if (Bukkit.getPluginManager().isPluginEnabled("TitleManager")) {
                thisPlugin.getLogger().info("TitleManager API is available. Setting it up.");
                FileConfiguration titleManagerConfiguration = new YamlConfiguration().loadConfiguration(new File(Bukkit.getPluginManager().getPlugin("TitleManager").getDataFolder(), "config.yml"));
                titleManagerTabHeaderEnabled = titleManagerConfiguration.getBoolean("tabmenu.enabled", false);
                if (titleManagerTabHeaderEnabled) {
                    titleManagerTabHeader = titleManagerConfiguration.getString("tabmenu.header", "");
                } else {
                    titleManagerTabHeader = "";
                }
                apiTitles = "TitleManager";
            } else {
                thisPlugin.getLogger().info("TitleManager is not installed. If you don't want to use titles, you can ignore this. You can suppress this warning by setting bossbar.enable: false in config.yml");
                apiTitles = "none";
            }
        } else {
            apiTitles = "none";
        }

    }

    static void doExportResource(String fileFromJar, String outFile) throws Exception {
        InputStream stream = null;
        OutputStream resStreamOut = null;
        try {
            stream = thisPlugin.getResource(fileFromJar);
            if(stream == null) {
                throw new Exception("Cannot get resource \"" + fileFromJar + "\" from jar.");
            }

            int readBytes;
            byte[] buffer = new byte[4096];
            resStreamOut = new FileOutputStream(thisPlugin.getDataFolder() + "/" + outFile);
            while ((readBytes = stream.read(buffer)) > 0) {
                resStreamOut.write(buffer, 0, readBytes);
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            stream.close();
            resStreamOut.close();
        }
    }

}
