package net.alexmcs.bukkit.serverstopper;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

import static net.alexmcs.bukkit.serverstopper.MainClass.thisPlugin;

/**
 * ServerStopper
 * net.alexmcs.bukkit.serverstopper.Translations
 * An extension of ConfigManager that does the same thing, but for translations.
 * This is in a separate class mostly just to make it easier to call from other classes to get strings.
 */
class Translations {

    // Wow! Look at all those strings!

    // See the comments in the bukkit-translations.yml resource for the commented key for information on each String.

    static String pluginName;
    static String version;
    static String website;

    static String broadcastPrefix; // chat-broadcast.prefix
    static String broadcastWarning; // chat-broadcast.warning
    static String broadcastNotify; // chat-broadcast.notify
    static String broadcastCancel; // chat-broadcast.cancel

    static String bossbarWarning; // bossbar-warning

    static String tmAbWarning; // titles.actionbar-warning
    static String tmSubWarning; // titles.subtitle-warning
    static String tmTitleWarning; // titles.title-warning
    static String tmTabFooterWarning; // titles.tabfoot-warning

    static String errorPrefix; // error.prefix
    static String errorPermission; // error.permissions.generic
    static String errorPermissionStop; // error.permissions.stop
    static String errorPermissionNow; // error.permissions.now
    static String errorPermissionCancel; // error.permissions.cancel
    static String errorPermissionTime; // error.permissions.time
    static String errorNotStopping; // error.not-stopping
    static String errorAlreadyStopping; // error.already-stopping
    static String errorTooShort; // error.too-short
    static String errorUnknownCommand; // error.unknown-command
    static String errorNotImplemented; // error.not-implemented

    static String help1; // help.1
    static String help2; // help.2
    static String help3; // help.3
    static String help4; // help.4
    static String help5; // help.5
    static String help6; // help.6
    static String help7; // help.7
    static String help8; // help.8
    static String help9; // help.9
    static String help10; // help.10

    static String aboutTranslator; // about.translator
    static String aboutHelp; // about.help
    static String aboutUri; // about.uri

    static String reloadSuccess; // reload

    // Store these values in ProxyHelper class?
    static String phBossbarWarning; // bungeecord.bossbar-warning
    static String phTmAbWarning; // bungeecord.actionbar-warning
    static String phTmSubWarning; // bungeecord.subtitle-warning
    static String phTmTitleWarning; // bungeecord.title-warning
    static String phTmTabFooterWarning; // bungeecord.tabfoot-warning

    static String consoleDisplayname; // console-displayname

    private static String translationVersion; // translation-version

    static void loadTranslations() {

        pluginName = thisPlugin.getName();
        version = thisPlugin.getDescription().getVersion();
        website = thisPlugin.getDescription().getWebsite();

        File translationFile = new File(thisPlugin.getDataFolder(), "translations.yml");

        // The ability to focus is something I do not possess.

        // If the translations file does not exist, create it.
        if (!translationFile.exists()) {
            thisPlugin.getLogger().warning("The translations file does not exist. Creating it.");
            try {
                ConfigManager.doExportResource("bukkit-translations.yml", "translations.yml");
            } catch (Exception ex) {
                throw new RuntimeException("Unable to export resource", ex);
            }
        }

        FileConfiguration translations = new YamlConfiguration().loadConfiguration(translationFile);

        String noTranslation = "&4&lERROR&r: Translation missing! Check translations.yml";

        broadcastPrefix = translations.getString("chat-broadcast.prefix", noTranslation);
        broadcastWarning = translations.getString("chat-broadcast.warning", noTranslation);
        broadcastNotify = translations.getString("chat-broadcast.notify", noTranslation);
        broadcastCancel = translations.getString("chat-broadcast.cancel", noTranslation);

        bossbarWarning = translations.getString("bossbar-warning", noTranslation);

        tmAbWarning = translations.getString("titles.actionbar-warning", noTranslation);
        tmSubWarning = translations.getString("titles.subtitle-warning", noTranslation);
        tmTitleWarning = translations.getString("titles.title-warning", noTranslation);
        tmTabFooterWarning = translations.getString("titles.tabfoot-warning", noTranslation);

        errorPrefix = translations.getString("error.prefix", noTranslation);
        errorPermission = translations.getString("error.permissions.generic", noTranslation);
        errorPermissionStop = translations.getString("error.permissions.stop", noTranslation);
        errorPermissionNow = translations.getString("error.permissions.now", noTranslation);
        errorPermissionCancel = translations.getString("error.permissions.cancel", noTranslation);
        errorPermissionTime = translations.getString("error.permissions.time", noTranslation);
        errorNotStopping = translations.getString("error.not-stopping", noTranslation);
        errorAlreadyStopping = translations.getString("error.already-stopping", noTranslation);
        errorTooShort = translations.getString("error.too-short", noTranslation);
        errorUnknownCommand = translations.getString("error.unknown-command", noTranslation);
        errorNotImplemented = translations.getString("error.not-implemented", noTranslation);

        help1 = translations.getString("help.1", noTranslation);
        help2 = translations.getString("help.2", noTranslation);
        help3 = translations.getString("help.3", noTranslation);
        help4 = translations.getString("help.4", noTranslation);
        help5 = translations.getString("help.5", noTranslation);
        help6 = translations.getString("help.6", noTranslation);
        help7 = translations.getString("help.7", noTranslation);
        help8 = translations.getString("help.8", noTranslation);
        help9 = translations.getString("help.9", noTranslation);
        help10 = translations.getString("help.10", noTranslation);

        aboutTranslator = translations.getString("about.translator", noTranslation);
        aboutHelp = translations.getString("about.help", noTranslation);
        aboutUri = translations.getString("about.uri", noTranslation);

        reloadSuccess = translations.getString("reload", noTranslation);

        phBossbarWarning = translations.getString("bungeecord.bossbar-warning", noTranslation);
        phTmAbWarning = translations.getString("bungeecord.actionbar-warning", noTranslation);
        phTmSubWarning = translations.getString("bungeecord.subtitle-warning", noTranslation);
        phTmTitleWarning = translations.getString("bungeecord.title-warning", noTranslation);
        phTmTabFooterWarning = translations.getString("bungeecord.tabfoot-warning", noTranslation);

        consoleDisplayname = translations.getString("console-displayname", noTranslation);

        translationVersion = translations.getString("translation-version", "NULL");

        // Throw a warning if configVersion is different.
        if (translationVersion.equals("SNAPSHOT")) {
            thisPlugin.getLogger().warning("Your translations file was generated by an unstable SNAPSHOT!");
            thisPlugin.getLogger().warning("SNAPSHOT translations are often undergoing big changes and may be invalid.");
            thisPlugin.getLogger().warning("If you see missing translations or ignored values, backup and delete your translations file to let the plugin regenerate it.");
        } else if (translationVersion.equals("NULL")) {
            thisPlugin.getLogger().warning("Your translations file version could not be detected!");
            thisPlugin.getLogger().warning("Your translations file may be invalid.");
            thisPlugin.getLogger().warning("If you see missing translations or ignored values, backup and delete your translations file to let the plugin regenerate it.");
        } else if (!translationVersion.equals("1.1")) {
            thisPlugin.getLogger().warning("Your translations file version is invalid!");
            thisPlugin.getLogger().warning("Your translations file may be invalid.");
            thisPlugin.getLogger().warning("If you see missing translations or ignored values, backup and delete your translations file to let the plugin regenerate it.");
        }


    }

}
