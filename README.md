# Minecraft ServerStopper
Have you ever wanted to restart your Minecraft server while people were online? Annoyed at manually warning them that
the server is going to restart soon? Worried they won't see your message? You need ServerStopper!

ServerStopper is a free plugin for Bukkit, Spigot, and Paper that replaces the built-in Minecraft /stop command with
it's own version that has a variety of options, including a customizable delay before the server stops, several
warnings in the minutes and seconds leading up to the server stop, and the ability to cancel a stop that's in progress!

## That sounds great. How do I install it?
It's easy, just download the jar file, drag it into plugins, and restart your server!
[You can download the release jar from here](https://gitlab.com/AlexMCS/ServerStopper/tags/)

## How do I use it?
There are a handful of simple commands. Just do <code>/stop</code> and your server will be scheduled to shut down in 1
minute.

Want to shut it down in more than a minute? Just do <code>/stop \<minutes\></code>.

Accidentally started a stop or want to cancel a stop that someone else started? Easy, just do <code>/stop cancel</code>

And you can even stop the server immediately like the original stop command with <code>/stop now</code>

And you can easily look up how to do these things in game with <code>/serverstopper help</code> or
<code>/stop help</code>

## I have a proxy. Can I use this?
Yes. ServerStopper works fine on servers behind proxies. You can also
[download ProxyStopper](https://gitlab.com/AlexMCS/ProxyStopper/), which implements the same concept in the BungeeCord
API, so you can stop your proxy in style too!

I also recommend installing [BungeeKick](https://www.spigotmc.org/resources/bungeekick.1310/) or
[MoveMeNow](https://www.spigotmc.org/resources/movemenow.17/) in your proxy so players will be moved to the lobby
instead of getting kicked out of your server when it restarts. I was going to implement this in ServerStopper itself,
but these plugins make for a better implementation, so I recommend using them.

## Other Information
Here's some more information for the nerdy types. :P

### Permissions
ServerStopper has only a small handful of permissions.

<code>serverstopper.info</code> - Get information about ServerStopper with /serverstopper
<br>
<code>serverstopper.help</code> - Get a quick tutorial on how to use ServerStopper with /serverstopper help
<br>
<code>serverstopper.stop</code> - Stop the server with /stop
<br>
<code>serverstopper.stop.time</code> - Set a custom amount of time with /stop \<minutes\>
<br>
<code>serverstopper.stop.now</code> - Stop the server immediately with /stop now
<br>
<code>serverstopper.cancel</code> - Cancel a server stop with /stop cancel
<br>
<code>serverstopper.admin</code> - All other permissions ___EXCEPT___ serverstopper.stop.now

By default, all permissions are given to ops.

### Bug Reports
Our issue tracker is [here](https://gitlab.com/AlexMCS/ServerStopper/issues). Please post any bugs you find there and
we'll look into fixing them.

### Development Builds

Want to try out a development build? You can download the latest development build at
[this link](https://gitlab.com/AlexMCS/ServerStopper/builds/artifacts/master/download?job=build).

Development builds should reflect the very latest code from this repository, but they may also carry strange bugs and
other problems.

Development builds will produce an artifacts.zip file. You can simply open it and find the jar file inside the target
folder.

### For Developers
You are free to make copies of, recompile, and redistribute ServerStopper however you please. That said, I want to ask
politely that you don't abuse this power and upload re-compiled versions with no changes or worse yet attempt to sell
copies of this plugin. Of course, there's nothing I can do to stop you, but that's just not a good thing to do.

By the way, maybe you wanted to [browse this repository](https://gitlab.com/AlexMCS/ServerStopper/tree/master)?

### Credits
This plugin was made by ColtonDRG for AlexMCS. :)

### License
Copyright &copy; 2016 AlexMCS (alexmcs.net)
<br>
ServerStopper is licensed under the MIT License. That means you're free to do whatever you want with it. See
[LICENSE.md](https://gitlab.com/AlexMCS/ServerStopper/blob/master/LICENSE.md) for more information.